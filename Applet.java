import javax.swing.JApplet;
import java.awt.Graphics;

public class Applet extends JApplet {
	public void paint(Graphics g){
		g.drawArc(0, 100, 100, 100, 270, 180);
		g.drawArc(100, 0, 100, 100, 180, 180);
		g.drawArc(200, 100, 100, 100, 90, 180);
		g.drawArc(100, 200, 100, 100, 0, 180);
		
		g.drawOval(100, 100, 100, 100);
		g.fillOval(125, 125, 50, 50);
	}
}
	