/*
* prompt user for test 1
 * save and print input test 1
 * prompt user for test 2
 * save and print input test 2
 * prompt user for test 3
 * save it to input test 3
 * prompt user for test 4
 * save it and input to test 4
 * prompt user for test 5 
 * save it and input to test 5
 * 
 * sum up the 5 tests; test 1 + test 2 + test 3 + test 4 + test 5
 * you average the sum * average and divide by 5
 * 
 * print sum
 * print average
 * 
 * check for min
 * print the min
 * check for the max
 * print the max
 * check for the median 
 * print the median
*/

import java.util.Arrays;
import java.util.Scanner;


public class Assignment2 {



	public static void main(String [] args)
	{
		int test1, test2, test3, test4, test5, sum, average;
		
		
		Scanner keyboard = new Scanner(System.in);

		System.out.println("Please enter your score of test 1.");
		test1 = keyboard.nextInt();
		
		System.out.println("Please enter your score of test 2.");
		test2 = keyboard.nextInt();
		
		System.out.println("Please enter your score of test 3.");
		test3 = keyboard.nextInt();
		
		System.out.println("Please enter your score of test 4.");
		test4 = keyboard.nextInt();
		
		System.out.println("Please enter your score of test 5.");
		test5 = keyboard.nextInt();
	
		sum = test1 + test2 + test3 + test4 + test5;
		average = sum / 5;

		System.out.println("The sum of five tests is " + sum);
		System.out.println("The average of five tests is " + average);

		int [] num = {test1,test2,test3,test4,test5};
		Arrays.sort(num);
		System.out.println("Min is " + num[0]);
		
		int max = 0;
		if ((test1 > test2) && (test1 > test3) && (test1 > test4) && (test1 > test5))
			max = test1;
			
		else if ((test2 > test3) && (test2 > test4) && (test2 > test5) && (test2 > test1))
			max = test2;
		
		else if ((test3 > test4) && (test3 > test5) && (test3 > test1) && (test3 > test2)) 
			max = test3;
		
		else if ((test4 > test5) && (test4 > test1) && (test4 > test2) && (test4 > test3))
			max = test4;
		
		else if ((test5 > test1) && (test5 > test2) && (test5 > test3) && (test5 > test4))
			max = test5;
		
		System.out.println("Max is " + max);
		System.out.println("Median is " + num[2]);
	}
}